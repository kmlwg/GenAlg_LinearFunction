#include "std_lib.h"

class Individual {
public:
	Individual() { };
	void   randGenes(const int& max, const int& l);
	void   calcFit();
    double getFit() { return fitness; }; // Returns the value of fitnnes of the Individual
    void   setProb(double p){ prob = p; };
    double getProb() { return prob; };
    void   genR() { r = rand() % 1000 / 1000.0; };
    double getR() { return r; };
    int    size() { return chromosome.size(); };
    void   setAll (int i, int x) { chromosome[i] = x; };
    int    getAll (int i) { return chromosome[i]; };
private:
	vector<int> chromosome;     // Genotype of the Indivdual
	double fitness;
	double prob;
	double r;       // random number <0,1>
};

// Generates the randomized genotype of the Individual
void Individual::randGenes(const int& max, const int& l){
	for (int i=0; i < l; i++){
		chromosome.push_back(rand() % max);
	}
}
// First calculates the objective function for Individual, than it's fitness
void Individual::calcFit(){
	int obj = chromosome[0] + 2 * chromosome[1] + 3 * chromosome[2] + 
		4 * chromosome[3] - 30;
	fitness = 1.0 / (1 + std::fabs(obj));
}

// The process of selection in the Population
// Using roulette wheel selection
void selection(vector<Individual>& popOld, vector<Individual>& popNew){
    // Calculate fittnes of each Individual in population
    for(int i = 0; i < popOld.size(); i++)
        popOld[i].calcFit();

    double totalFit = 0;   // Value of the total fitness of the population
    for (int i = 0; i < popOld.size(); i++)
        totalFit += popOld[i].getFit();
    
    // Calculate probability to stay for each Individual
    for (int i = 0; i < popOld.size(); i++){
        popOld[i].setProb(popOld[i].getFit() / totalFit);
    }

    // Calculate cummulative probability
    vector<double> cummProb(popOld.size());
    for (int i = 0; i < popOld.size(); i++){
        for (int j = 0; j <= i; j++){
            cummProb[i] += popOld[j].getProb();
        }
    }
    
    for (int i = 0; i < popOld.size(); i++) {
        popOld[i].genR();
    }

    // Process of selection
    int popNew_iter = 0;
    for (int i = 0; i < popOld.size(); i++){
        int k = i;
        if (i == 35) {
           // cout << 
        }
        while (true){
            if (popOld[i].getR() > cummProb[k] && popOld[i].getR() < cummProb[k+1]){
                popNew[popNew_iter] = popOld[k];
                popNew_iter++;
                break;
            }
            else if (popOld[i].getR() < cummProb[k]){
                if (k == 0){
                    popNew[popNew_iter] = popOld[k];
                    popNew_iter++;
                    break;
                }
                k--;
            }
            else if (popOld[i].getR() > cummProb[k+1]){
                if (k + 1 >= popOld.size() - 1){
                    popNew[popNew_iter] = popOld[k+1];
                    popNew_iter++;
                    break;
                }
                k++;
            }
            else break;
        }
    }

}
// The process od crossing over
void crossOver(vector<Individual>& popNew, const double& cr){
    vector<int> parIndex;           // holds an index of where the parent in population is
    vector<Individual> parents;     // holds copies of parents

    // Generate new random numbers <0,1> for each individual
    for (int i = 0; i < popNew.size(); i++){
        popNew[i].genR();
        if (popNew[i].getR() < cr){
            parIndex.push_back(i);
            parents.push_back(popNew[i]);
        }
    }

    // Generate random number to determine where to cut genotype
    vector<int> c(parents.size());
    for (int i = 0; i < c.size(); i++)
        c[i] = rand() % popNew[0].size();

    // Process of cross-over
    for (int i = 0; i < parents.size(); i++){
        if (i == parents.size() - 1){
            for (int j = 0; j < c[i]; j++)
                popNew[parIndex[i]].setAll(j, parents[i].getAll(j));
            for (int j = c[i]; j < popNew[0].size(); j++)
                popNew[parIndex[i]].setAll(j, parents[0].getAll(j));
        }
        else {
            for (int j = 0; j < c[i]; j++)
                popNew[parIndex[i]].setAll(j, parents[i].getAll(j));
            for (int j = c[i]; j < popNew[0].size(); j++)
                popNew[parIndex[i]].setAll(j, parents[i+1].getAll(j));
        }
    }
}
// The procces of mutation
void mutation(vector<Individual>& popNew, const double& mr, const int &max){
    // Total number of allels
    int totalGene =  popNew.size() * popNew[0].size();
    int mutNum = 0;
    if ( rand() % totalGene + 1 < mr * totalGene)
        mutNum = mr * totalGene;
    
    vector<int> randInd(mutNum);
    vector<int> randGene(mutNum);
    vector<int> randAll(mutNum);

    for (int i = 0; i < mutNum; i++){
        randInd[i]  = rand() % popNew.size();
        randGene[i] = rand() % popNew[0].size();
        randAll[i]  = rand() % max;
    }
    for (int i = 0; i < mutNum; i++)
        popNew[randInd[i]].setAll(randGene[i], randAll[i]);
}

// Print Population to csv file
void print(vector<Individual>& pop, ofstream& ostr1,ofstream& ostr2, int i){

    double avg = 0;
    for (int j = 0; j < pop.size(); j++)
        avg += pop[j].getFit();
    avg /= pop.size();

    ostr1 << "Generation;" << i << "; Avg Fit;" << avg << "\n";

    if (avg >= 0.9 and i != 0){
        ostr2 << "Generation;" << i << ";Avg Fit;" << avg <<"\n";
        ostr2 << "Lp.;Fitness;Allel 0;Allel 1;Allel 2;Allel 3\n";
        for (int j = 0; j < pop.size(); j++){
            ostr2 << j << ";" << pop[j].getFit() << ";" <<pop[j].getAll(0)<<";"
                << pop[j].getAll(1) << ";" << pop[j].getAll(2) << ";"
                << pop[j].getAll(3) << "\n"; 
        }
        ostr2 << "\n";
    }
}

int main()
{
    srand(time(NULL));

    const int    popSize = 1000;        // Size of the population
    const int    genNum  = 100000;       // Number of generations
    const double cr      = 0.20;       // Cross-over rate
    const double mr      = 0.03;       // Mutation rate  
    const int    lenChr  = 4;          // Number of allels in chromosome
    const int    max     = 10;         // Max value of coeficients in function
    
    
    // Declare two popultions, popNew replaces popOld at the end of each
    // iteration
    vector<Individual> popOld(popSize);
    vector<Individual> popNew(popSize);


    ofstream ostr1;
    ostr1.open("result.csv");
    ofstream ostr2;
    ostr2.open("generation.csv");

    for (int i = 0; i < popSize; i++)
        popOld[i].randGenes(max, lenChr);

    for (int i = 0; i < genNum; i++){
        if (i == 0) print(popOld, ostr1,ostr2, i);
        selection(popOld, popNew);
        crossOver(popNew, cr);
        mutation(popNew, mr, max);
        print(popNew, ostr1,ostr2, i + 1);
        popOld = popNew;
        //popNew.clear();
        //popNew.resize(0);
        //cout << i << endl;
        
    }

    ostr1.close();
    ostr2.close();
    return 0;
}