import matplotlib.pyplot as plt
import csv

x = []
y = []

with open('result.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=';')
    for row in plots:
        x.append(int(row[1]))
        y.append(float(row[3]))

plt.plot(x,y)
plt.xlabel('Evarage fitness function')
plt.ylabel('Generation')
plt.legend()
plt.show()